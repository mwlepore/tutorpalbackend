# cse442TeamJamBackend
Server side repo for Tutor App

Requires the following python packages:

django

djangorestframework

django-cors-headers

django-rest-framework-social-oauth2


Contents of conf file I used to host the project on development server.

	< VirtualHost *:80 >

	ServerName warmachine.cse.buffalo.edu
	ServerAlias warmachine.cse.buffalo.edu
	ServerAdmin webmaster@localhost	

	DocumentRoot /var/www/tutor
	WSGIScriptAlias / /var/www/tutor/tutor/wsgi.py
	WSGIPassAuthorization On

	ErrorLog /var/www/logs/error.log
	CustomLog /var/www/logs/custom.log combined

	< /VirtualHost >

Deployed Front End will be expecting API to be located at fury.cse.buffalo.edu/tutorapp

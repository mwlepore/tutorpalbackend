from django.contrib import admin
from .models import UserProfile, Courses, Message

admin.site.register(UserProfile)
admin.site.register(Courses)
admin.site.register(Message)

# Register your models here.

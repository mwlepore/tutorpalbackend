# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-16 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('UserProfiles', '0005_auto_20161116_1201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='firstname',
            field=models.CharField(max_length=25),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='lastname',
            field=models.CharField(max_length=25),
        ),
    ]

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Message(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    sender = models.ForeignKey(User,null=False, related_name='sender')
    recipient = models.ForeignKey(User,null=False, related_name='recipient')
    body = models.CharField(max_length=1024)
    class Meta:
        ordering = ('created',)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    lastname = models.CharField(max_length=25)
    firstname = models.CharField(max_length=25)
    aboutme = models.CharField(max_length=128,default='default')
    subject = models.CharField(max_length=16,default='None')
    rate = models.PositiveIntegerField(default=0)
    location = models.PositiveIntegerField(default=0)
    language = models.CharField(max_length=16,default='english')
    image_url = models.CharField(max_length=256,default='None')
    facebook_id = models.CharField(max_length=64, default='None')
    latitude = models.FloatField(max_length=64, default=0)
    longitude = models.FloatField(max_length=64, default=0)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ('lastname','firstname')

    def __str__(self):
        return self.firstname + " " + self.lastname


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Courses(models.Model):
    coursename = models.CharField(max_length=256, default='')
    subject = models.CharField(max_length=32, default='none')

    def __str__(self):
        return self.coursename

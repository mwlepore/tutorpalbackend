from rest_framework import serializers
from .models import UserProfile
from django.contrib.auth.models import User
from .models import Courses
from .models import Message

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'
                #(
                  #'pk',
                  #'user',
                  #'lastname',
                  #'firstname',
                  #'aboutme',
                  #'subject',
                  #'rate',
                  #'location',
                  #'language',
                  #'image_url',
                  #'facebook_id',
                  #)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = ('coursename', 'subject')

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

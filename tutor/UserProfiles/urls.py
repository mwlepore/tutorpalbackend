from django.conf.urls import url
from UserProfiles import views


urlpatterns = [
    url(r'^userprofiles$', views.UserList.as_view()),
    url(r'^edit/me', views.EditCurrentUser.as_view()),
    url(r'^userprofiles/new',views.NewUser.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$',views.Users.as_view()),
    url(r'^users/me', views.CurrentUser.as_view()),
    url(r'^profiles/me', views.CurrentProfile.as_view()),
    url(r'^courses$', views.CourseList.as_view()),
    url(r'^messages$', views.MessageList.as_view()),
    url(r'^mymessages$', views.MyMessages.as_view()),
    url(r'^sendmessage$', views.SendMessage.as_view()),
    url(r'^sentmessages$', views.SentMessages.as_view()),
    url(r'^inbox$', views.ReceivedMessages.as_view()),

]
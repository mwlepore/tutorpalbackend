from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import UserProfile
from .serializers import UserProfileSerializer
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth.models import User
from .serializers import UserSerializer
from rest_framework import permissions
from rest_framework.response import Response
from .models import Courses
from .serializers import CourseSerializer,MessageSerializer
from .models import Message
from django.db.models.query_utils import Q


class MyMessages(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        messagelist = Message.objects.filter(Q(sender=request.user)|Q(recipient=request.user))
        serializer = MessageSerializer(messagelist,many=True)
        return Response(serializer.data)


class ReceivedMessages(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        messagelist = Message.objects.filter(Q(recipient=request.user))
        serializer = MessageSerializer(messagelist,many=True)
        return Response(serializer.data)


class SentMessages(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        messagelist = Message.objects.filter(Q(sender=request.user))
        serializer = MessageSerializer(messagelist,many=True)
        return Response(serializer.data)

class SendMessage(generics.CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessageList(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('sender','recipient',)


class CurrentProfile(generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        profile = request.user.profile
        serializer = UserProfileSerializer(profile)
        return Response(serializer.data)


class UserList(generics.ListAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('subject',)


class EditCurrentUser(generics.UpdateAPIView):
    def put(self, request, *args, **kwargs):
        profile = request.user.profile
        serializer = UserProfileSerializer(profile ,data=request.data,partial=True)
        serializer.is_valid()
        serializer.save()
        return Response(serializer.data)


class NewUser(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class Users(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CurrentUser(generics.RetrieveAPIView):
    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)


class CourseList(generics.ListAPIView):
    queryset = Courses.objects.all()
    serializer_class = CourseSerializer
